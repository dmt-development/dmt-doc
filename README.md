# DMT-doc

[![logo](https://gitlab.com/uploads/-/system/project/avatar/33580822/DMT_Logo_wText.png)](https://gitlab.com/uploads/-/system/project/avatar/33580822/DMT_Logo_wText.png)

This is the documentation to the Device Modeling Toolkit (DMT) developed by SemiMod GmbH

## Authors

- M. Müller | Markus.Mueller@semimod.de
- M. Krattenmacher | Mario.Krattenmacher@semimod.de

## License

This project is licensed under GLP-v3-or-later