x\_verify module
=======================


Additional verification plots are available in

.. toctree::
    :maxdepth: 1
    :glob:

    x_verify_plots

XVerify Module Documentation
----------------------------
.. automodule:: DMT.extraction.x_verify
    :members:
    :undoc-members:
    :show-inheritance:
 