get\_poly\_region module
=========================

.. automodule:: DMT.extraction.get_poly_region
    :members:
    :undoc-members:
    :show-inheritance:
