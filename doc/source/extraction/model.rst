model module
=======================

Implemented models
----------------------------

.. toctree::
    :maxdepth: 1
    :glob:

    models/*

Model Module documentation
------------------------------

.. automodule:: DMT.extraction.model
    :members:
    :undoc-members:
    :show-inheritance:
