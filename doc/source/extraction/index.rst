Extraction framework
====================

Inside this DMT module all extra classes and functions needed to implement and extract a compact model. The basic strategy is:

1. Implement your model card class as a subclass of :class:`~DMT.core.mcard.MCard`
2. Implement your default circuits for this :class:`~DMT.core.mcard.MCard` class using :class:`~DMT.core.circuit.Circuit`
3. Implement :class:`~DMT.extraction.x_step.XStep` classes to extract a small set of parameters of your model, either using :class:`~DMT.extraction.model.Model` or verilogae and the verilog-A code
4. Run your steps in :class:`~DMT.extraction.xtraction.Xtraction` groups. Each :class:`~DMT.extraction.xtraction.Xtraction` gets an own section in the documentation using :class:`~DMT.extraction.docu_xtraction.DocuXtraction`.
5. Verify your extraction using several :class:`~DMT.extraction.x_verify.XVerify` instances for all the different figures of merit.
6. Create the PDK (if needed)

In order to test your extraction, either good measurement data is needed or you simulate your model and use this as the data to extract from. :mod:`~DMT.extraction.generate_virtual_data` helps you with that.

This is the full source code documentation about all the classes and functions inside DMT.extraction.

.. toctree::
    :maxdepth: 2

    model
    x_step
    xtraction
    generate_virtual_data
    docu/docu_xtraction