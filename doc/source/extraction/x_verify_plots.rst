Additional verification plots
=============================

.. automodule:: DMT.extraction.x_verify_plots
    :members:
    :undoc-members:
    :show-inheritance:
 