.. _dut_lib:

DutLib module
=======================

Is used to manage measurement data of many devices in a unified way. DutLib offers methods to import many files at once and then save and load them inside the Python/DMT environment efficiently.

In order to start the import process the data should be saved in this file structure::

    meas/
    ├── DeviceA/
    │   ├── T1/
    │   │   ├── meas_1
    │   │   └── meas_2
    │   └── T2/
    │       ├── meas_1
    │       └── meas_2
    └── DeviceA/
        ├── T1/
        │   ├── meas_1
        │   └── meas_2
        └── T2/
            ├── meas_1
            └── meas_2


DutLib module code documentation
--------------------------------

.. automodule:: DMT.core.dut_lib
    :members:
    :undoc-members:
    :show-inheritance:
