Juncap capacitance extraction step
==================================

Extraction step for the juncap capacitance of the PSP MOSFET compact model. Each of the two junctions (drain-buld and source-bulk) has three components (bottom, STI and gate edge).

.. automodule:: DMT.psp.x_juncap_cap
    :members:
    :undoc-members:
    :show-inheritance:
