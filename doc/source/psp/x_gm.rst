Gm extraction step
==================

Extraction step for the transconductance of the PSP MOSFET compact model.

.. automodule:: DMT.psp.x_gm
    :members:
    :undoc-members:
    :show-inheritance:
