Ig extraction step
==================

Extraction step for the gate current of the PSP MOSFET compact model.

.. automodule:: DMT.psp.x_ig
    :members:
    :undoc-members:
    :show-inheritance:
