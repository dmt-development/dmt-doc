PSP default circuits
====================

DMT.psp supplies some default circuits ready to be used to extract and verify the PSP model.

The functions to directly interact with LaTeX are:

.. automodule:: DMT.psp.psp_default_circuits
    :members:
    :undoc-members:
    :show-inheritance: