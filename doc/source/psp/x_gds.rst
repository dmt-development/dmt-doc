Gds extraction step
===================

Extraction step for the output conductance of the PSP MOSFET compact model.

.. automodule:: DMT.psp.x_gds
    :members:
    :undoc-members:
    :show-inheritance:
