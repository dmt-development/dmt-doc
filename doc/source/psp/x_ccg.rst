Ccg extraction step
===================

Extraction step for the channel-gate capacitance of the PSP MOSFET compact model.

.. automodule:: DMT.psp.x_ccg
    :members:
    :undoc-members:
    :show-inheritance:
