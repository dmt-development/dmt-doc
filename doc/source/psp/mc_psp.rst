PSP Model card
==============

Model card class for a PSP compact model instance.

.. automodule:: DMT.psp.mc_psp
    :members:
    :undoc-members:
    :show-inheritance: