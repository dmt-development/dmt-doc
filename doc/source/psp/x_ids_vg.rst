Ids over Vg extraction step
===========================

Extraction step for the drain-source current over gate voltage of the PSP MOSFET compact model.

.. automodule:: DMT.psp.x_ids_vg
    :members:
    :undoc-members:
    :show-inheritance:
