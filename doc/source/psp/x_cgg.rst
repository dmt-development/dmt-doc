Cgg extraction step
===================

Extraction step for the gate-source and the gate-drain capacitance of the PSP MOSFET compact model.

.. automodule:: DMT.psp.x_cgg
    :members:
    :undoc-members:
    :show-inheritance: