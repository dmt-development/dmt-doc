Ib extraction step
==================

Extraction step for the bulk current of the PSP MOSFET compact model.

.. automodule:: DMT.psp.x_ib
    :members:
    :undoc-members:
    :show-inheritance:
