Rs extraction step
==================

Extraction step for the series resistances of the PSP MOSFET compact model using y-parameters.

.. automodule:: DMT.psp.x_y_rs
    :members:
    :undoc-members:
    :show-inheritance:
